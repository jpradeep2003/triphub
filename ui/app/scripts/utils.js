var utilsModule = angular.module('utilsModule', []);

utilsModule.service('imageService', function() {

	this.getAgentImageUrl = function(imageObj, width, height) {

		if (!imageObj) {
			return null;
		}

		if (imageObj.provider == 'cloudinary') {
			var imageUrl = 'http://res.cloudinary.com/' + imageObj.account
					+ '/image/upload/c_fill,w_' + width + ',h_' + height + '/'
					+ imageObj.id + '.jpg';

			return imageUrl;
		} else {
			console.log('image provider undefined');
		}
	}

	this.getLogoImageUrl = function(imageObj, width, height) {

		if (!imageObj) {
			return null;
		}

		if (imageObj.provider == 'cloudinary') {
			var imageUrl = 'http://res.cloudinary.com/' + imageObj.account
					+ '/image/upload/c_limit,w_' + width + ',h_' + height + '/'
					+ imageObj.id + '.jpg';

			return imageUrl;
		} else {
			console.log('image provider undefined');
		}
	}

	this.getBannerImageUrl = function(imageObj, width, height) {

		if (!imageObj) {
			return null;
		}

		if (imageObj.provider == 'cloudinary') {
			var imageUrl = 'http://res.cloudinary.com/' + imageObj.account
					+ '/image/upload/c_limit,w_' + width + ',h_' + height + '/'
					+ imageObj.id + '.jpg';

			return imageUrl;
		} else {
			console.log('image provider undefined');
		}
	}

});

utilsModule.service('formDataHelper', function() {

	this.formDataObject = function(data, headersGetter) {
		var fd = new FormData();
		angular.forEach(data, function(value, key) {
			fd.append(key, value);
		});

		var headers = headersGetter();
		delete headers['Content-Type'];

		return fd;
	};
});

utilsModule
		.service(
				'validationPatterns',
				function() {
					return {
						'name' : /^[ A-Za-z0-9'_.-]*$/,
						'email' : /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
						'password' : /^(\d+[a-z]|[a-z]+\d)[a-z\d]*$/i, // alphanumeric
																		// and
																		// no
																		// space
						'phone' : /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/i,
						'singleWord' : /^\s*\w*\s*$/, // for company name
						'address' : /(?=.*[@!#\$\^%&*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]+?).*[^_\W]+?.*/,
						'phoneOrEmail' : /^((([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$)|(^[0-9]{10,10}$)/,
						'link' : /^[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/
					};
				});
