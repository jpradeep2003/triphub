'use strict';

angular.module('guestApp')
    .controller('RegisterCtrl', ['$scope', '$http', function($scope, $http) {


        $scope.alerts = [];
        $scope.loading = false;

        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

        $scope.form = {};
        $scope.signupWizard = {};
        $scope.signupWizard.state = "step1";
        $scope.sendVerificationEmail = function(user) {

            if ($scope.form.signup_form.$valid) {
                $scope.loading = true;
                $http.get('/verifyEmailForUserSignup', {
                    params: {
                        'email': user.email
                    }
                }).success(function(response) {
                    if (response.errorCode) {
                        $scope.loading = false;
                        $scope.alerts.push({
                            type: 'danger',
                            msg: response.errorMessage
                        });
                    } else {
                        $scope.loading = false;
                        $scope.signupWizard.state = "step2";
                    }
                }).error(function(data, status, headers, config) {
                    $scope.loading = false;
                    if (status < 400) {
                        $scope.alerts = [];
                        $scope.alerts.push({
                            type: 'danger',
                            msg: 'Server unavailable momentarilly. Please try again after some time.'
                        });
                    }
                });
            } else {
                $scope.submitted = true;
            }
        }
        $scope.submitCode = function(user) {
            $scope.submitted = false;

            if ($scope.form.signup_form_step2.$valid) {
                $http.get('/verifyEmail', {
                    params: {
                        'email': user.email,
                        'token': user.token
                    }
                }).success(function(response) {
                    if (response.errorCode) {
                        $scope.alerts = [];
                        $scope.alerts.push({
                            type: 'danger',
                            msg: response.errorMessage
                        });
                    } else {
                        console.log('Email verified successfully');
                        $scope.signupWizard.state = "step3";
                        $scope.alerts = [];
                    }
                }).error(function(data, status, headers, config) {
                    if (status < 400) {
                        $scope.alerts = [];
                        $scope.alerts.push({
                            type: 'danger',
                            msg: 'Server unavailable momentarilly. Please try again after some time.'
                        });
                    }
                });
            } else {
                $scope.submitted = true;
            }

        };

        $scope.submitSignupForm = function(user, profile) {
            $scope.submitted = false;

            if ($scope.form.signup_form_step3.$valid) {

                if (user.password != user.password2) {
                    $scope.alerts.push({
                        type: 'danger',
                        msg: 'Your passwords do not match. Please try again.'
                    });
                    return;
                }

                $scope.loading = true;

                profile.type = user.type;

                var sumbitForm = {};
                sumbitForm.email = user.email;
                sumbitForm.token = user.token;
                sumbitForm.password = user.password;
                sumbitForm.profile = JSON.stringify(profile);

                $http({
                    url: "/signup",
                    data: sumbitForm,
                    method: 'POST',
                    transformRequest: $scope.formDataHelper.formDataObject
                }).success(function(response) {

                    if (response.errorCode) {
                        $scope.alerts = [];
                        $scope.alerts.push({
                            type: 'danger',
                            msg: response.errorMessage
                        });
                        return;
                    }

                    console.log('signup successful. Proceeding to signin');

                    $http.get('/neevLogin', {
                        params: {
                            'username': user.email,
                            'password': user.password
                        }
                    }).success(function(response) {
                        $scope.loading = false;
                        if (response.errorCode) {
                            $scope.alerts = [];
                            $scope.alerts.push({
                                type: 'danger',
                                msg: response.errorMessage
                            });
                            return;
                        }

                        console.log('login successful');
                        window.location.href = "/dashboard";

                    }).error(function(data, status, headers, config) {
                        $scope.loading = false;
                        if (status < 400) {
                            $scope.alerts = [];
                            $scope.alerts.push({
                                type: 'danger',
                                msg: 'Server unavailable momentarilly. Please try again after some time.'
                            });
                        }
                    });
                }).error(function(data, status, headers, config) {
                    if (status < 400) {
                        $scope.alerts = [];
                        $scope.alerts.push({
                            type: 'danger',
                            msg: 'Server unavailable momentarilly. Please try again after some time.'
                        });
                    }
                });

            } else {
                $scope.submitted = true;
            }

        };

    }]);