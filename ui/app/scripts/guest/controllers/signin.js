'use strict';

angular
    .module('guestApp')
    .controller(
        'LoginCtrl', [
            '$scope',
            '$http',
            function($scope, $http) {

                $scope.alerts = [];
                $scope.loading = false;
                $scope.closeAlert = function(index) {
                    $scope.alerts.splice(index, 1);
                };
                $scope.form = {};

                $scope.submit = function(user) {
                    if ($scope.form.signin_form.$valid) {
                        $scope.loading = true;
                        $http
                            .get('/neevLogin', {
                                params: {
                                    'username': user.user,
                                    'password': user.password
                                }
                            })
                            .success(
                                function(response) {
                                    if (response.errorCode) {
                                        $scope.loading = false;
                                        $scope.alerts
                                            .push({
                                                type: 'danger',
                                                strongContent: 'Incorrect credentials ! ',
                                                msg: 'Please re-enter your information, The information you entered is incorrect.'
                                            });
                                    } else {
                                        window.location.href = "/dashboard";
                                    }
                                })
                            .error(
                                function(data, status,
                                    headers, config) {
                                    $scope.loading = false;
                                    if (status < 400) {
                                        $scope.alerts = [];
                                        $scope.alerts
                                            .push({
                                                type: 'danger',
                                                msg: 'Server unavailable momentarilly. Please try again after some time.'
                                            });
                                    }
                                });
                    } else {
                        $scope.submitted = true;
                    }
                }
            }
        ]);