'use strict';

var guestApp = angular.module('guestApp', [ 'ngRoute', 'ui.bootstrap',
		'utilsModule' ]);

guestApp.run([ '$route', '$rootScope', 'validationPatterns', 'formDataHelper',
		function($route, $rootScope, $validationPatterns, formDataHelper) {

			$route.reload();
			$rootScope.validationPatterns = $validationPatterns;
			$rootScope.formDataHelper = formDataHelper;

		} ]);

guestApp.config([ '$routeProvider', function($routeProvider) {

	$routeProvider.otherwise({
		redirectTo : '/signin'
	});

	$routeProvider.when('/signin', {
		templateUrl : 'views/guest/signin.html'
	}).when('/signup', {
		templateUrl : 'views/guest/signup.html'
	});

} ]);
