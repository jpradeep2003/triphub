'use strict';

var agentApp = angular.module('agentApp', ['ngRoute', 'daterangepicker', 'angularMoment', 'ui.bootstrap', 'utilsModule']);

agentApp.run(['$route', '$rootScope', 'validationPatterns', 'formDataHelper',
    function($route, $rootScope, $validationPatterns, formDataHelper) {

        $route.reload();
        $rootScope.validationPatterns = $validationPatterns;
        $rootScope.formDataHelper = formDataHelper;

    }
]);

agentApp.controller('mainController', ['$scope', '$http',
    function($scope, $http) {

        var initialize = function() {

            $scope.getUserInfo();
        }

        $scope.getUserInfo = function() {

            $http.get('/getUserInfo').success(function(userInfo) {
                console.log('getUserInfo ' + JSON.stringify(userInfo));
                $scope.userInfo = userInfo;
            }).error(function(data, status, headers, config) {
                $scope.getServerError(data, status, headers, config);
            });
        }

        $scope.logout = function() {

            $http.get('/neevLogout', {}).success(function(response) {
                if (response.errorCode) {
                    // To Be done....
                } else {
                    console.log('logout successful');
                    window.location.href = "/";
                }
            }).error(function(data, status, headers, config) {
                $scope.getServerError(data, status, headers, config);
            });

        };


        initialize();

    }
]);


agentApp.controller('agentController', ['$scope', '$http',
    function($scope, $http) {

        var initialize = function() {

            getSkus();
            getInventoryItems();
            getBookings();

            $scope.addSkuForm = {};
            $scope.addItemForm = {};
            $scope.addSkuForm.type = 'ROOM';

        }

        var getSkus = function() {

            $http.get('/getSkus').success(function(response) {
                $scope.skus = response;
            }).error(function(data, status, headers, config) {
                $scope.getServerError(data, status, headers, config);
            });
        }

        $scope.addSku = function() {

            return $http.post("/addSku", {
                type: $scope.addSkuForm.type,
                roomHotelName: $scope.addSkuForm.roomHotelName,
                roomNumber: $scope.addSkuForm.roomNumber
            }, {
                headers: {
                    'Content-Type': 'application/json'
                }
            }).success(function(response) {

                console.log('addSku DONE');
                $scope.addSkuForm = {};
                getSkus();
                return response;
            });

        }

        var getInventoryItems = function() {

            $http.get('/getInventoryItems').success(function(response) {
                $scope.inventoryItems = response;
            }).error(function(data, status, headers, config) {
                $scope.getServerError(data, status, headers, config);
            });
        }


        $scope.addInventoryItem = function() {

            var startDate = moment($scope.addItemForm.dateRange.startDate).startOf('day');
            var endDate = moment($scope.addItemForm.dateRange.endDate).startOf('day');

            console.log('addInventoryItem  startDate ' + moment(startDate).format('MM/DD/YYYY'));
            console.log('addInventoryItem  endDate   ' + moment(endDate).format('MM/DD/YYYY'));

            var dates = [];
            var currentDate = startDate;
            while (currentDate <= endDate) {
                console.log('currentDate ' + moment(currentDate).format('MM/DD/YYYY'));
                console.log('currentDate ' + moment(currentDate));
                dates.push('' + moment(currentDate));
                currentDate = moment(currentDate).add(1, 'days');
            }




            return $http.post("/addInventoryItem", {
                skuId: $scope.addItemForm.skuId,
                dates: dates,
                quantity: $scope.addItemForm.quantity
            }, {
                headers: {
                    'Content-Type': 'application/json'
                }
            }).success(function(response) {

                console.log('addInventory DONE');
                $scope.addItemForm = {};
                getInventoryItems();
                return response;
            });

        }

        $scope.dateRangePickerOpts = {
            format: 'MMMM D, YYYY',
            startDate: moment(),
            endDate: moment(),
            minDate: moment(),
            maxDate: moment().add('months', 6),
            dateLimit: {
                days: 15
            },
            showWeekNumbers: true,
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            ranges: {
                'Today': [moment(), moment()],
                'Tomorrow': [moment().add(1, 'days'), moment().add(1, 'days')],
                'Next 7 Days': [moment().add(1, 'days'), moment().add(7, 'days')],
                'Next Month': [moment().add(1, 'months').startOf('month'), moment().add(1, 'months').endOf('month')],
            }
        };


        var getBookings = function() {

            $http.get('/getAgentBookings').success(function(response) {
                $scope.bookings = response;
            }).error(function(data, status, headers, config) {
                $scope.getServerError(data, status, headers, config);
            });
        }


        initialize();

    }
]);