'use strict';

var customerApp = angular.module('customerApp', ['ngRoute', 'daterangepicker', 'angularMoment', 'ui.bootstrap', 'utilsModule']);

customerApp.run(['$route', '$rootScope', 'validationPatterns', 'formDataHelper',
    function($route, $rootScope, $validationPatterns, formDataHelper) {

        $route.reload();
        $rootScope.validationPatterns = $validationPatterns;
        $rootScope.formDataHelper = formDataHelper;

    }
]);

customerApp.controller('mainController', ['$scope', '$http',
    function($scope, $http) {

        var initialize = function() {

            $scope.getUserInfo();
        }

        $scope.getUserInfo = function() {

            $http.get('/getUserInfo').success(function(userInfo) {
                console.log('getUserInfo ' + JSON.stringify(userInfo));
                $scope.userInfo = userInfo;
            }).error(function(data, status, headers, config) {
                $scope.getServerError(data, status, headers, config);
            });
        }

        $scope.logout = function() {

            $http.get('/neevLogout', {}).success(function(response) {
                if (response.errorCode) {
                    // To Be done....
                } else {
                    console.log('logout successful');
                    window.location.href = "/";
                }
            }).error(function(data, status, headers, config) {
                $scope.getServerError(data, status, headers, config);
            });

        };


        initialize();

    }
]);

customerApp.controller('customerController', ['$scope', '$http',
    function($scope, $http) {

        var initialize = function() {

            getBookings();
            $scope.searchForm = {};
            $scope.searchForm.type = 'ROOM';
            $scope.activeTab = 'Search';
        }

        var getBookings = function() {

            $http.get('/getCustomerBookings').success(function(response) {
                $scope.bookings = response;
            }).error(function(data, status, headers, config) {
                $scope.getServerError(data, status, headers, config);
            });
        }

        $scope.search = function() {

            var startDate = moment($scope.searchForm.dateRange.startDate).startOf('day');
            var endDate = moment($scope.searchForm.dateRange.endDate).startOf('day');

            console.log('search  startDate ' + moment(startDate).format('MM/DD/YYYY'));
            console.log('search  endDate   ' + moment(endDate).format('MM/DD/YYYY'));


            return $http.post("/search", {
                type: $scope.searchForm.type,
                startDate: '' + moment(startDate),
                endDate: '' + moment(endDate)
            }, {
                headers: {
                    'Content-Type': 'application/json'
                }
            }).success(function(response) {

                console.log('search DONE');
                $scope.matchingItems = response;
                $scope.activeTab = 'Results';
                return response;
            });

        }

        $scope.dateRangePickerOpts = {
            format: 'MMMM D, YYYY',
            startDate: moment(),
            endDate: moment(),
            minDate: moment(),
            maxDate: moment().add('months', 6),
            dateLimit: {
                days: 10
            },
            showWeekNumbers: true,
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to '
        };


        $scope.book = function(skuId) {

            return $http.post("/book", {
                itemId: skuId,
                quantity: 1
            }, {
                headers: {
                    'Content-Type': 'application/json'
                }
            }).success(function(response) {

                console.log('booking done');
                getBookings();
                $scope.activeTab = 'Bookings';
                return response;
            });

        }

        initialize();

    }
]);