'use strict';

var adminApp = angular.module('adminApp', ['ngRoute', 'angularMoment', 'ui.bootstrap', 'utilsModule']);

adminApp.run(['$route', '$rootScope', 'validationPatterns', 'formDataHelper',
    function($route, $rootScope, $validationPatterns, formDataHelper) {

        $route.reload();
        $rootScope.validationPatterns = $validationPatterns;
        $rootScope.formDataHelper = formDataHelper;

    }
]);

adminApp.controller('mainController', ['$scope', '$http',
    function($scope, $http) {

        var initialize = function() {

            $scope.getUserInfo();
        }

        $scope.getUserInfo = function() {

            $http.get('/getUserInfo').success(function(userInfo) {
                console.log('getUserInfo ' + JSON.stringify(userInfo));
                $scope.userInfo = userInfo;
            }).error(function(data, status, headers, config) {
                $scope.getServerError(data, status, headers, config);
            });
        }

        $scope.logout = function() {

            $http.get('/neevLogout', {}).success(function(response) {
                if (response.errorCode) {
                    // To Be done....
                } else {
                    console.log('logout successful');
                    window.location.href = "/";
                }
            }).error(function(data, status, headers, config) {
                $scope.getServerError(data, status, headers, config);
            });

        };


        initialize();

    }
]);


adminApp.controller('adminController', ['$scope', '$http',
    function($scope, $http) {

        var initialize = function() {

            getBookings();

        }

        var getBookings = function() {

            $http.get('/getAdminBookings').success(function(response) {
                $scope.bookings = response;
            }).error(function(data, status, headers, config) {
                $scope.getServerError(data, status, headers, config);
            });
        }


        initialize();

    }
]);