package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Morphia;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import play.mvc.With;
import triphub.booking.manager.BookingManager;
import triphub.booking.model.Booking;
import triphub.inventory.manager.ItemManager;
import triphub.inventory.model.Item;
import triphub.sku.manager.SkuManager;
import triphub.sku.model.Sku;
import triphub.sku.model.SkuDAO;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.imisno.neev.auth.Secured;
import com.imisno.neev.db.DBJoin;
import com.imisno.neev.user.model.User;
import com.imisno.neev.util.ApplicationException;
import com.imisno.neev.util.ExceptionHelper;
import com.imisno.neev.util.NeevExceptionCodes;
import com.imisno.neev.util.VerboseAction;
import com.mongodb.DBObject;

@With({ VerboseAction.class, ExceptionHelper.class })
public class AgentController extends Controller {

	public static final Logger LOGGER = LoggerFactory
			.getLogger(AgentController.class);

	@Security.Authenticated(Secured.class)
	@BodyParser.Of(BodyParser.Json.class)
	public static Result addSku() throws ApplicationException {

		User userEntity = (User) ctx().args.get(Secured.USER_KEY);

		JsonNode json = request().body().asJson();

		Sku sku = new Sku();
		sku.createdByUserId = userEntity.getId();
		sku.type = json.findPath("type").textValue();

		if (sku.type.equals(Sku.SKU_TYPE_ROOM)) {
			sku.roomHotelName = json.findPath("roomHotelName").textValue();
			sku.roomNumber = json.findPath("roomNumber").textValue();

		} else if (sku.type.equals(Sku.SKU_TYPE_PROGRAM)) {
			sku.programTitle = json.findPath("programTitle").textValue();
			sku.programDescripton = json.findPath("programDescripton")
					.textValue();
		}

		sku = SkuManager.addSku(sku);

		return ok(Json.toJson(sku));
	}

	@Security.Authenticated(Secured.class)
	public static Result getSkus() throws JsonGenerationException,
			JsonMappingException, IOException {

		User userEntity = (User) ctx().args.get(Secured.USER_KEY);

		List<Sku> accounts = SkuManager.findUserSkus(userEntity.getId());

		return ok(Json.toJson(accounts));
	}

	@Security.Authenticated(Secured.class)
	@BodyParser.Of(BodyParser.Json.class)
	public static Result addInventoryItem() throws ApplicationException {

		JsonNode json = request().body().asJson();

		JsonNode datesNode = json.get("dates");
		TypeReference<List<String>> typeRef = new TypeReference<List<String>>() {
		};
		ObjectMapper mapper = new ObjectMapper();
		List<String> list;
		try {
			list = mapper.readValue(datesNode.traverse(), typeRef);
			for (String date : list) {

				Item item = new Item();
				item.skuId = json.findPath("skuId").textValue();

				Sku sku = SkuDAO.getDAO().findById(item.skuId);
				item.type = sku.type;
				item.createdByUserId = sku.createdByUserId;
				item.quantity = json.findPath("quantity").asInt();
				item.available = item.quantity;
				
				// TODO: copy all searchable attributes of sku to the item.
				item.date = Long.parseLong(date);
				LOGGER.trace("addInventory...... " + item.date);
				ItemManager.addItem(item);
			}

		} catch (IOException e) {
			e.printStackTrace();
			throw new ApplicationException(NeevExceptionCodes.INVALID_INPUT,
					e.getMessage());
		}

		return ok();
	}

	@Security.Authenticated(Secured.class)
	public static Result getInventoryItems() throws JsonGenerationException,
			JsonMappingException, IOException {

		User userEntity = (User) ctx().args.get(Secured.USER_KEY);

		List<Item> items = ItemManager.findUserItems(userEntity.getId());

		Morphia morphia = new Morphia();
		
		List<DBObject> itemObjs = new ArrayList<DBObject>();
		for(Item item : items){
			DBObject itemObj = morphia.toDBObject(item);
			itemObj.put("id", item.getId());
			itemObjs.add(itemObj);
		}
		
		DBJoin.resolveReferences(itemObjs, true, "skuId", "Sku", "roomHotelName, roomNumber");

		return ok(Json.toJson(itemObjs));
	}

	@Security.Authenticated(Secured.class)
	public static Result getAgentBookings() throws JsonGenerationException,
			JsonMappingException, IOException {

		User userEntity = (User) ctx().args.get(Secured.USER_KEY);

		List<Booking> bookings = BookingManager.findAgentBookings(userEntity
				.getId());

		Morphia morphia = new Morphia();

		List<DBObject> bookingObjs = new ArrayList<DBObject>();
		for(Booking booking : bookings){
			DBObject bookingObj = morphia.toDBObject(booking);
			bookingObjs.add(bookingObj);
		}
		
		DBJoin.resolveReferences(bookingObjs, true, "itemId", "Item", "skuId, date");
		DBJoin.resolveReferences(bookingObjs, true, "forUserId", "User",
				"userId");

		return ok(Json.toJson(bookingObjs));
	}

}
