package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Morphia;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import play.mvc.With;
import triphub.booking.manager.BookingManager;
import triphub.booking.model.Booking;
import triphub.inventory.manager.InsufficientQuantityException;
import triphub.inventory.manager.ItemManager;
import triphub.inventory.model.Item;
import triphub.sku.model.Sku;
import triphub.sku.model.SkuDAO;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.imisno.neev.auth.Secured;
import com.imisno.neev.db.DBJoin;
import com.imisno.neev.user.model.User;
import com.imisno.neev.util.ApplicationException;
import com.imisno.neev.util.ExceptionHelper;
import com.imisno.neev.util.VerboseAction;
import com.mongodb.DBObject;

@With({ VerboseAction.class, ExceptionHelper.class })
public class CustomerController extends Controller {

	public static final Logger LOGGER = LoggerFactory
			.getLogger(CustomerController.class);

	@Security.Authenticated(Secured.class)
	@BodyParser.Of(BodyParser.Json.class)
	public static Result search() throws ApplicationException {

		JsonNode json = request().body().asJson();
		long startDate = json.findPath("startDate").asLong();
		long endDate = json.findPath("endDate").asLong();
		String type = json.findPath("type").asText();

		List<Item> items = ItemManager.findItems(type, startDate, endDate);

		Morphia morphia = new Morphia();

		List<DBObject> itemObjs = new ArrayList<DBObject>();
		for (Item item : items) {
			DBObject itemObj = morphia.toDBObject(item);
			itemObj.put("id", item.getId());
			itemObjs.add(itemObj);
		}

		DBJoin.resolveReferences(itemObjs, true, "skuId", "Sku",
				"roomHotelName, roomNumber");

		return ok(Json.toJson(itemObjs));
	}

	@Security.Authenticated(Secured.class)
	@BodyParser.Of(BodyParser.Json.class)
	public static Result addBooking() throws ApplicationException {

		User userEntity = (User) ctx().args.get(Secured.USER_KEY);

		JsonNode json = request().body().asJson();

		String itemId = json.findPath("itemId").textValue();
		int quantity = json.findPath("quantity").asInt();

		try {
			// If there is insufficent quantity, an excption would be thrown
			// here
			ItemManager.book(itemId, quantity);

			Booking booking = new Booking();
			booking.itemId = itemId;
			booking.forUserId = userEntity.getId();
			booking.quantity = json.findPath("quantity").asText();

			Item item = ItemManager.findById(itemId);
			booking.itemCreatedByUserId = item.createdByUserId;

			Sku sku = SkuDAO.getDAO().findById(item.skuId);
			booking.summary = sku.roomHotelName + " - " + sku.roomNumber;

			booking = BookingManager.addBooking(booking);

			return ok(Json.toJson(booking));
		} catch (InsufficientQuantityException excp) {
			throw excp;
		}
	}

	@Security.Authenticated(Secured.class)
	public static Result getCustomerBookings() throws JsonGenerationException,
			JsonMappingException, IOException {

		User userEntity = (User) ctx().args.get(Secured.USER_KEY);

		List<Booking> bookings = BookingManager.findCustomerBookings(userEntity
				.getId());

		Morphia morphia = new Morphia();

		List<DBObject> bookingObjs = new ArrayList<DBObject>();
		for (Booking booking : bookings) {
			DBObject bookingObj = morphia.toDBObject(booking);
			bookingObjs.add(bookingObj);
		}

		DBJoin.resolveReferences(bookingObjs, true, "itemId", "Item",
				"skuId, date");

		return ok(Json.toJson(bookingObjs));
	}

}
