package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Morphia;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import play.mvc.With;
import triphub.booking.manager.BookingManager;
import triphub.booking.model.Booking;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.imisno.neev.auth.Secured;
import com.imisno.neev.db.DBJoin;
import com.imisno.neev.util.ExceptionHelper;
import com.imisno.neev.util.VerboseAction;
import com.mongodb.DBObject;

@With({ VerboseAction.class, ExceptionHelper.class })
public class AdminController extends Controller {

	public static final Logger LOGGER = LoggerFactory
			.getLogger(AdminController.class);

	@Security.Authenticated(Secured.class)
	public static Result getAdminBookings() throws JsonGenerationException,
			JsonMappingException, IOException {

		List<Booking> bookings = BookingManager.findBookings();

		Morphia morphia = new Morphia();

		List<DBObject> bookingObjs = new ArrayList<DBObject>();
		for (Booking booking : bookings) {
			DBObject bookingObj = morphia.toDBObject(booking);
			bookingObjs.add(bookingObj);
		}

		DBJoin.resolveReferences(bookingObjs, true, "itemId", "Item",
				"skuId, date");
		DBJoin.resolveReferences(bookingObjs, true, "forUserId", "User",
				"userId");

		return ok(Json.toJson(bookingObjs));
	}

}
