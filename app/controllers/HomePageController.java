package controllers;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import play.mvc.With;

import com.imisno.neev.auth.Secured;
import com.imisno.neev.user.model.User;
import com.imisno.neev.util.ApplicationException;
import com.imisno.neev.util.ExceptionHelper;
import com.imisno.neev.util.VerboseAction;
import com.mongodb.DBObject;

@With({ VerboseAction.class, ExceptionHelper.class })
public class HomePageController extends Controller {

	public static final Logger LOGGER = LoggerFactory
			.getLogger(HomePageController.class);

	public static Result home() {

		String username = ctx().session().get("user");

		if (StringUtils.isBlank(username)) {
			return redirect("/ui/guest.html");
		} else {
			// If login session is found, redirect to user dashboard
			return redirect("/dashboard");
		}
	}

	@Security.Authenticated(Secured.class)
	public static Result dashboard() throws ApplicationException {

		User user = (User) ctx().args.get(Secured.USER_KEY);

		DBObject obj = user.getProfile();
		Object userTypeObj = obj.get("type");

		if (userTypeObj != null) {
			String userType = userTypeObj.toString();

			LOGGER.trace("home: userType is " + userType);
			switch (userType) {
			default:
			case "CUSTOMER":
				return redirect("/ui/customer.html");
			case "ADMIN":
				return redirect("/ui/admin.html");
			case "AGENT":
				return redirect("/ui/agent.html");
			}

		} else {

			LOGGER.trace("home: userType is null");
			return redirect("/ui/customer.html");
		}

	}

}