package triphub.sku.model;

import java.util.List;

import org.bson.types.ObjectId;

import com.imisno.neev.db.NeevBasicDAO;

public class SkuDAO extends NeevBasicDAO<Sku, ObjectId> {

	public static SkuDAO getDAO() {
		return new SkuDAO();
	}

	public List<Sku> findUserSkus(String createdByUserId) {

		return find(createQuery().filter("createdByUserId = ", createdByUserId))
				.asList();
	}

}
