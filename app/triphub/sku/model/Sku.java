package triphub.sku.model;

import org.mongodb.morphia.annotations.Entity;

import com.imisno.neev.db.NeevEntity;

@Entity(noClassnameStored = true)
public class Sku extends NeevEntity {

	public static final String SKU_TYPE_ROOM = "ROOM";
	public static final String SKU_TYPE_PROGRAM = "PROGRAM";

	public String createdByUserId;

	public String type;

	public String roomHotelName;
	public String roomNumber;

	public String programTitle;
	public String programDescripton;

	@Override
	public String toString() {
		return "Sku [createdByUserId=" + createdByUserId + ", type=" + type
				+ ", roomHotelName=" + roomHotelName + ", roomNumber="
				+ roomNumber + ", programTitle=" + programTitle
				+ ", programDescripton=" + programDescripton + "]";
	}

}
