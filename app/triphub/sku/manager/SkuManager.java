package triphub.sku.manager;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import triphub.sku.model.Sku;
import triphub.sku.model.SkuDAO;

import com.imisno.neev.util.ApplicationException;

public class SkuManager {

	public static final Logger LOGGER = LoggerFactory
			.getLogger(SkuManager.class);

	public static Sku addSku(Sku sku) throws ApplicationException {

		SkuDAO.getDAO().save(sku);
		return sku;
	}

	public static List<Sku> findUserSkus(String createdByUserId) {

		return SkuDAO.getDAO().findUserSkus(createdByUserId);
	}

}
