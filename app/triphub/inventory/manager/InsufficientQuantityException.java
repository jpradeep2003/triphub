package triphub.inventory.manager;

import com.imisno.neev.util.ApplicationException;

public class InsufficientQuantityException extends ApplicationException {

	private static final long serialVersionUID = 1L;

	public static final String INSUFFICIENT_QUANTITY = "INSUFFICIENT_QUANTITY";

	public InsufficientQuantityException(String message) {
		super(INSUFFICIENT_QUANTITY, message);
	}
}
