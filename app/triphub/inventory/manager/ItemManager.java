package triphub.inventory.manager;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import triphub.inventory.model.Item;
import triphub.inventory.model.ItemDAO;

import com.imisno.neev.util.ApplicationException;

public class ItemManager {

	public static final Logger LOGGER = LoggerFactory
			.getLogger(ItemManager.class);

	public static Item addItem(Item item) throws ApplicationException {

		ItemDAO.getDAO().save(item);
		return item;
	}

	public static List<Item> findUserItems(String createdByUserId) {

		return ItemDAO.getDAO().findUserItems(createdByUserId);
	}

	public static Item findById(String itemId) {

		return ItemDAO.getDAO().findById(itemId);
	}

	public static void book(String itemId, int quantity)
			throws InsufficientQuantityException {
		ItemDAO.getDAO().book(itemId, quantity);
	}

	public static List<Item> findItems(String type, long startDate, long endDate) {

		return ItemDAO.getDAO().findItems(type, startDate, endDate);

	}

}
