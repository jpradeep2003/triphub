package triphub.inventory.model;

import org.mongodb.morphia.annotations.Entity;

import com.imisno.neev.db.NeevEntity;

@Entity(noClassnameStored = true)
public class Item extends NeevEntity {

	public String skuId;

	public long date;

	// Following fields to allow search on this table.
	public String createdByUserId;
	public String type;

	public int quantity;
	public int booked;
	public int available;

	@Override
	public String toString() {
		return "Item [skuId=" + skuId + ", date=" + date + ", createdByUserId="
				+ createdByUserId + ", type=" + type + ", quantity=" + quantity
				+ ", booked=" + booked + ", available=" + available + "]";
	}

}
