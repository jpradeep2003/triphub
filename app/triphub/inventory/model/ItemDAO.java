package triphub.inventory.model;

import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

import triphub.inventory.manager.InsufficientQuantityException;

import com.imisno.neev.db.NeevBasicDAO;

public class ItemDAO extends NeevBasicDAO<Item, ObjectId> {

	public static ItemDAO getDAO() {
		return new ItemDAO();
	}

	public List<Item> findUserItems(String createdByUserId) {

		return find(createQuery().filter("createdByUserId = ", createdByUserId))
				.asList();
	}

	public void book(String itemId, int quantity)
			throws InsufficientQuantityException {

		LOGGER.trace("book for itemId " + itemId);

		// Get the latest version of item from db
		Item item = findById(itemId);

		if (quantity <= (item.available)) {

			Query<Item> query = createQuery();
			query.field("id").equal(new ObjectId(item.getId()));

			UpdateOperations<Item> ops = ItemDAO.getDAO().getUpdateOperations();
			ops.set("booked", item.booked + quantity);
			ops.set("available", item.available - quantity);

			ItemDAO.getDAO().update(query, ops, false);
		} else {
			throw new InsufficientQuantityException("Item " + item.getId());
		}

	}

	public List<Item> findItems(String type, long startDate, long endDate) {

		LOGGER.trace("findItems.... type " + type);
		LOGGER.trace("findItems.... startDate " + startDate);
		LOGGER.trace("findItems.... endDate " + endDate);

		return find(
				createQuery().filter("type = ", type)
						.filter("date >=", startDate).filter("available >", 0)
						.filter("date <=", endDate).order("-timeCreated"))
				.asList();
	}

}
