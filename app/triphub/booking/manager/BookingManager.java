package triphub.booking.manager;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import triphub.booking.model.Booking;
import triphub.booking.model.BookingDAO;

import com.imisno.neev.util.ApplicationException;

public class BookingManager {

	public static final Logger LOGGER = LoggerFactory
			.getLogger(BookingManager.class);

	public static Booking addBooking(Booking booking)
			throws ApplicationException {

		BookingDAO.getDAO().save(booking);
		return booking;
	}

	public static List<Booking> findBookings() {

		return BookingDAO.getDAO().findBookings();
	}

	public static List<Booking> findAgentBookings(String itemCreatedByUserId) {

		return BookingDAO.getDAO().findAgentBookings(itemCreatedByUserId);
	}

	public static List<Booking> findCustomerBookings(String forUserId) {

		return BookingDAO.getDAO().findCustomerBookings(forUserId);
	}

}
