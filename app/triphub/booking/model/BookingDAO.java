package triphub.booking.model;

import java.util.List;

import org.bson.types.ObjectId;

import com.imisno.neev.db.NeevBasicDAO;

public class BookingDAO extends NeevBasicDAO<Booking, ObjectId> {

	public static BookingDAO getDAO() {
		return new BookingDAO();
	}

	public List<Booking> findBookings() {

		return find().asList();
	}

	public List<Booking> findAgentBookings(String itemCreatedByUserId) {

		return find(
				createQuery().filter("itemCreatedByUserId = ",
						itemCreatedByUserId)).asList();
	}

	public List<Booking> findCustomerBookings(String forUserId) {
		return find(createQuery().filter("forUserId = ", forUserId)).asList();
	}

}
