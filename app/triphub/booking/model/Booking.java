package triphub.booking.model;

import org.mongodb.morphia.annotations.Entity;

import com.imisno.neev.db.NeevEntity;

@Entity(noClassnameStored = true)
public class Booking extends NeevEntity {

	public String itemId;
	public String itemCreatedByUserId;
	public String forUserId;

	public String summary;
	public String quantity;

	@Override
	public String toString() {
		return "Booking [itemId=" + itemId + ", itemCreatedByUserId="
				+ itemCreatedByUserId + ", forUserId=" + forUserId
				+ ", quantity=" + quantity + "]";
	}

}
