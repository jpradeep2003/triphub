import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.Application;
import play.Play;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.imisno.neev.user.signup.SignupManager;
import com.imisno.neev.user.signup.SignupManagerImpl;

public class Global extends play.GlobalSettings {

	public static final Logger LOGGER = LoggerFactory.getLogger(Global.class);

	private Injector injector;

	@Override
	public void beforeStart(Application app) {
		LOGGER.trace("BEFORE START ........................");

		LOGGER.trace("MONGOHQ_URL = "
				+ Play.application().configuration().getString("MONGOHQ_URL"));
		LOGGER.trace("application.baseUrl = "
				+ Play.application().configuration()
						.getString("application.baseUrl"));
		LOGGER.trace("testserver.baseUrl = "
				+ Play.application().configuration()
						.getString("testserver.baseUrl"));
	}

	@Override
	public void onStop(Application app) {
		LOGGER.trace("IN STOP........................");
	}

	@Override
	public void onStart(Application application) {
		injector = Guice.createInjector(new AbstractModule() {
			@Override
			protected void configure() {
				bind(SignupManager.class).to(SignupManagerImpl.class);
			}
		});

	}

	@Override
	public <T> T getControllerInstance(Class<T> aClass) throws Exception {
		return injector.getInstance(aClass);
	}
}
