import com.tuplejump.sbt.yeoman.Yeoman

name := "triphub"

version := "1.0.1"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  "neev" % "neev_2.10" % "1.0.25" 
)     

resolvers += Resolver.url("Imisno Bitbucket Play Repository",
			url("https://bitbucket.org/api/1.0/repositories/jpradeep2003/releases/raw/master/"))(Resolver.ivyStylePatterns)

play.Project.playJavaSettings  ++ Yeoman.yeomanSettings
